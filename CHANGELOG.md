# ABM Change Log

## [0.0.4] - 2017-28-08
- Modified endless loop check with a check for clients > 0

## [0.0.3] - 2017-28-08
### Fixed
- Endless loop with clients that were both unable to target one another

## [0.0.2] - 2017-28-08
### Added
- First Release

## This CHANGELOG follows http://keepachangelog.com/en/0.3.0/
### CHANGELOG legend

- Added: for new features.
- Changed: for changes in existing functionality.
- Deprecated: for once-stable features removed in upcoming releases.
- Removed: for deprecated features removed in this release.
- Fixed: for any bug fixes.
- Security: to invite users to upgrade in case of vulnerabilities.
- [YANKED]: a tag too signify a release to be avoided.
